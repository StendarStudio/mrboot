package Stendar;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import static java.lang.Thread.sleep;

class MrBoot {

    public static void main(String[] args) throws InterruptedException {

        WebDriver driver;

        System.setProperty("webdriver.chrome.driver", "C:/drivers/chromedriver.exe");
        driver = new ChromeDriver();
        driver.navigate().to("http://przyklady.javastart.pl/test/full_form.html");

        WebElement userNamieField = driver.findElement(By.id("username"));
        sleep(2000);
        userNamieField.sendKeys("Reksio");

        driver.findElement(By.cssSelector("input[value='male']"));

        WebElement maleRadioButton = driver.findElement(By.cssSelector("input[value='male']"));
        sleep(2000);
        maleRadioButton.click();

        driver.findElement(By.cssSelector("input[value='pizza']"));
        driver.findElement(By.cssSelector("input[value='spaghetti']"));
        driver.findElement(By.cssSelector("input[value='hamburger']"));

        WebElement pizzaCheckBox = driver.findElement(By.cssSelector("input[value='pizza']"));
        WebElement spaghettiCheckBox = driver.findElement(By.cssSelector("input[value='spaghetti']"));
        WebElement hamburgerCheckBox = driver.findElement(By.cssSelector("input[value='hamburger']"));

        pizzaCheckBox.click();
        sleep(1000);
        spaghettiCheckBox.click();
        sleep(1000);
        hamburgerCheckBox.click();
        sleep(1000);

        WebElement uploadFilePicker = driver.findElement(By.id("upload_file"));
        uploadFilePicker.sendKeys("C:/Users/sten/Desktop/Przechwytywanie.JPG");


        WebElement countryWebElement = driver.findElement(By.id("country"));
        Select countryDropDown = new Select(countryWebElement);
        countryDropDown.selectByIndex(1);
        sleep(3000);

        WebElement saveButton = driver.findElement(By.id("confirm_button"));
        saveButton.click();

    }
}



